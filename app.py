from flask import Flask, jsonify, request
from flask_cors import CORS
import flask_sqlalchemy as sqlalchemy
from flask import redirect, url_for
from flask import Response
from flask_login import LoginManager, UserMixin, current_user, login_user, login_required
from getpass import getpass
from flask import current_app
import datetime


app = Flask(__name__)
login_manager = LoginManager()
login_manager.init_app(app)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///sqlalchemy-demo.db'

db = sqlalchemy.SQLAlchemy(app)


#the Course database will include coures, having atriubutes of a course title, number, professor, and TAs.
#We plan to store profile information for each professor and student,
#we may need to create another class and database to store their information.
class Course(db.Model):
    course_name = db.Column(db.String(128), primary_key = True)
    title = db.Column(db.String(128))
    description = db.Column(db.String(128))


class Student(db.Model):
    ID = db.Column(db.Integer, primary_key = True)
    name_first = db.Column(db.String(64))
    name_last = db.Column(db.String(64))
    email = db.Column(db.String(128))
    phoneNumber = db.Column(db.Integer, primary_key = True)
    major = db.Column(db.String(64))
    GPA = db.Column(db.Float)
    gradDate = db.Column(db.String(128))
    password = db.Column(db.String(128))
    courses_applied = db.relationship('CourseTA', backref='applicant', lazy='dynamic')


class Instructor(db.Model):
    ID = db.Column(db.Integer, primary_key = True)
    name_first = db.Column(db.String(64))
    name_last = db.Column(db.String(64))
    email = db.Column(db.String(128))
    phoneNumber = db.Column(db.Integer, primary_key = True)
    department = db.Column(db.String(64))
    password = db.Column(db.String(128))


class CourseTA(db.Model):
    SID = db.Column(db.Integer, db.ForeignKey('student.ID'), primary_key=True)
    FID = db.Column(db.Integer, db.ForeignKey('instructor.ID'), primary_key = True)
    course_name = db.Column(db.String(128), db.ForeignKey('course.course_name'), primary_key=True)
    courseInfo = db.relationship('Course', backref='courseTACourseInfo', lazy=True,)
    grade = db.Column(db.Float)
    semester = db.Column(db.String(128))
    application_date = db.Column(db.String(64))
    prior_TA = db.Column(db.Boolean)
    application_status = db.Column(db.String(64), default = "under review")
    instructorInfo = db.relationship('Instructor', backref='instructorinfo', lazy='joined')
    studentInfo = db.relationship('Student', backref='StudentInfo', lazy='joined')

class CourseInstructor(db.Model):
    FID = db.Column(db.Integer, db.ForeignKey('instructor.ID'), primary_key=True)
    course_name = db.Column(db.String(128), db.ForeignKey('course.course_name'), primary_key=True)
    courseInfo = db.relationship('Course', backref='courseInstructorCourseInfo', lazy='joined')
    has_TA = db.Column(db.Boolean, default=False)
    instructorInfo = db.relationship('Instructor', backref='InstructorInfo', lazy='joined')
    # name_first = db.Column(db.String(128), db.ForeignKey('instructor.name_first'))
    # name_last = db.Column(db.String(128), db.ForeignKey('instructor.name_last'))


# class User(UserMixin):
#     # proxy for a database of users
#     user_database = {"JohnDoe": ("JohnDoe", "John"),
#                "JaneDoe": ("JaneDoe", "Jane")}
#
#     def __init__(self, username, password):
#         self.id = username
#         self.password = password
#
#     @classmethod
#     def get(cls,id):
#         return cls.user_database.get(id)
#
#
# @login_manager.request_loader
# def load_user(request):
#     token = request.headers.get('Authorization')
#     if token is None:
#         token = request.args.get('token')
#
#     if token is not None:
#         username,password = token.split(":") # naive token
#         user_entry = User.get(username)
#         if (user_entry is not None):
#             user = User(user_entry[0],user_entry[1])
#             if (user.password == password):
#                 return user
#     return None
#
#
# @app.route("/protected/",methods=["GET"])
# @login_required
# def protected():
#     return Response(response="Hello Protected World!", status=200)



class User(db.Model):
    ID = db.Column(db.Integer, primary_key = True)
    password = db.Column(db.String(64))
#    authenticated = db.Column(db.Boolean, default = False)

    def is_active(self):
        return True
    def get_id(self):
        return self.ID
    def is_authenticated(self):
        return self.authenticated
    def is_anonymous(self):
        return False


base_url = '/api/'

# #the following comment block was to help Christopher understand how to preform login routes.
# class User(UserMixin):
#     #proxy for a database of users
#     user_database = {"JohnDoe": ("JohnDoe", "John"), "JaneDoe": ("JaneDoe", "Jane")}
#
#     def __init__(self, username, password):
#         self.id = username
#         self.password = password
#
#     @classmethod
#     def get(cls, id):
#         return cls.user_database.get(id)
#
# @login_manager.request_loader
# def load_user(request):
#     token = request.headers.get('Authorization')
#     if token is None:
#         token = request.args.get('token')
#
#     if token is not None:
#         username.password = token.split(":") #naive token
#         user_entry = User.get(username)
#         if (user_entry is not None):
#             user = User(user_entry[0], user_entry[1])
#             if (user.password == password):
#                 return user
#     return None
#
# @app.route(base_url + 'logintest', methods=['GET'])
# def index2():
#     return Response(response="hello World!", status=200)
#
# @app.route(base_url + 'protected', methods=['GET'])
# @login_required
# def protected():
#     return Response(response="Hello Protected World!", status=200)


#this should redirect to the home page or the login page depending on status
@app.route('/')
def begin():
    print("webpage")
    return redirect(url_for(filename='index.html'))


#this is the home screen, we should be able to view the schedule of classes here and sort by various methods
@app.route(base_url + 'home')
def index():
    space = request.args.get('sort', None)

    if space is None:
        return "Must provide sort", 500

    count = request.args.get('count',None)
    order_by = request.args.get('order_by',None)

    #we're going to order the schedule in the homepage
    if order_by == 'title':
        sort = Course.title
    elif order_by == 'courseNo':
        sort = Course.courseNo
    elif order_by == 'instructor':
        sort = Course.instructor_name_last
    elif order_by == 'TAcount':
        sort = Course.TA_list
    else: sort = None

    #order before we limit our search results
    if order_by == None:
        query = Course.query


#POSTMAN test conditions.
#http://127.0.0.1:5000/api/create_student
# {
#   "ID":12345678,
#   "name_first":"Christopher",
#   "name_last":"Roberts",
#   "email":"chris.bruh.roberts@wsu.edu",
#   "phoneNumber": "5555555555",
#   "major": "CptS",
#   "GPA": "3.0",
#   "gradDate": "1/1/11",
#   "password": "1234"
# }
#these routes are to register new students
@app.route(base_url + 'create_student', methods=['POST'])
def create_student():
    student = Student(**request.json)
    db.session.add(student)
    db.session.commit()
    db.session.refresh(student)
    return jsonify({'status': 1, 'student': student_row_to_obj(student)}), 200

# route for editing student account
@app.route(base_url + 'edit_student/<int:id>', methods=['POST'])
def edit_student(id):
    student = Student(**request.json)
    row = Student.query.filter_by(ID=id).first()
    row.name_first = student.name_first
    row.name_last = student.name_last
    row.ID = student.ID
    row.email = student.email
    row.phoneNumber = student.phoneNumber
    row.major = student.major
    row.GPA = student.GPA
    row.gradDate = student.gradDate
    db.session.commit()
    return jsonify({'status': 1, 'student': student_row_to_obj(row)}), 200


#POSTMAN test conditions.
#http://127.0.0.1:5000/api/create_instructor
# {
#   "ID":12345678,
#   "name_first":"Christopher",
#   "name_last":"Roberts",
#   "email":"chris.bruh.roberts@wsu.edu",
#   "phoneNumber": "5555555555",
#   "department": "CptS",
#   "password": "1234"
# }
#this route is to the create instructor page
@app.route(base_url + 'create_instructor', methods=['POST'])
def create_instructor():
    instructor = Instructor(**request.json)
    db.session.add(instructor)
    db.session.commit()
    db.session.refresh(instructor)
    return jsonify({'status': 1, 'instructor': instructor_row_to_obj(instructor)}), 200


# route for editing instructor account
@app.route(base_url + 'edit_instructor/<int:id>', methods=['POST'])
def edit_instructor(id):
    instructor = Instructor(**request.json)
    row = Instructor.query.filter_by(ID=id).first()
    row.name_first = instructor.name_first
    row.name_last = instructor.name_last
    row.ID = instructor.ID
    row.email = instructor.email
    row.phoneNumber = instructor.phoneNumber
    row.department = instructor.department
    db.session.commit()
    return jsonify({'status': 1, 'instructor': instructor_row_to_obj(row)}), 200

#POSTMAN test conditions
#http://127.0.0.1:5000/api/create_course
# {
# 	"course_name": "CptS_32342",
# 	"title": "this is a 100 year course",
# 	"description": "you would actually die before you could take this course"
# }
#this is a post route, allows us to add new courses to the course database
@app.route(base_url + 'create_course', methods=['POST'])
def create_course():
    course = Course(**request.json)
    db.session.add(course)
    db.session.commit()
    db.session.refresh(course)
    return jsonify({'status': 1, 'course': course_row_to_obj(course)}), 200


#POSTMAN test conditions
#http://127.0.0.1:5000/api/create_courseTA
# {
# 	"SID": 12345678,
# 	"FID": 98765432,
# 	"course_name": "CptS_322",
# 	"grade": 4.0,
# 	"semester": "Fall2018",
# 	"application_date": "10/31/2018",
# 	"prior_TA": true,
# 	"application_status": "application accepted"
# }
#this route is to add a course with a TA to the CourseTA table in the database
@app.route(base_url + 'create_courseTA', methods=['POST'])
def create_courseTA():
    courseTA = CourseTA(**request.json)
    db.session.add(courseTA)
    db.session.commit()
    db.session.refresh(courseTA)
    return jsonify({'status': 1, 'courseTA': courseTA_row_to_obj(courseTA)}), 200


#http://127.0.0.1:5000/api/create_courseInstructor
# {
#     "FID": 1234,
#     "course_name": "CptS_32342"
# }
@app.route(base_url + 'create_courseInstructor', methods=['POST'])
def create_courseInstructor():
    courseInstructor = CourseInstructor(**request.json)
    db.session.add(courseInstructor)
    db.session.commit()
    db.session.refresh(courseInstructor)
    return jsonify({'status': 1, 'courseInstructor': courseInstructor_row_to_obj(courseInstructor)}), 200


#this route returns all entries in the courseTA table
@app.route(base_url + 'get_courseTA', methods=['GET'])
def get_courseTA():
    query = CourseTA.query
    query = query.all()
    result = []
    for row in query:
        result.append(
            courseTA_row_to_obj(row)
        )
    return jsonify({"status": 1, "CourseTA": result}), 200


#this route searches the students by ID and returns a match
@app.route(base_url +'get_student/<int:id>', methods=['GET'])
def get_student(id):
    row = Student.query.filter_by(ID=id).first()
    return jsonify({'Student': student_row_to_obj(row)}), 200


#this route searches the instructors by ID and returns a match
@app.route(base_url +'get_instructor/<int:id>', methods=['GET'])
def get_instructor(id):
    row = Instructor.query.filter_by(ID=id).first()
    return jsonify({'Instructor': instructor_row_to_obj(row)}), 200


#this route gets a list of all the courses offered
@app.route(base_url+'get_all_courses', methods=['GET'])
def get_course():
    query = Course.query
    query = query.all()
    result = []
    for row in query:
        result.append(
            course_row_to_obj(row)
        )
    return jsonify({"status": 1, "Course": result}), 200


# route to get all the courses that an instructor teaches.
@app.route(base_url +'get_Courseinstructor/<int:id>', methods=['GET'])
def get_courseInstructor(id):
    query = CourseInstructor.query.filter_by(FID=id).join(Course, Course.course_name == CourseInstructor.course_name).all()
    result = []
    for row in query:
        result.append(
            courseInstructor_row_to_obj(row)
        )
    return jsonify({"status": 1, 'CourseInstructor': result}), 200


# route to get courses from the CourseTA table using the instructor ID
@app.route(base_url + 'get_instructor_courses/<int:id>', methods=['GET'])
def get_instructor_courses(id):
    query = CourseTA.query.filter_by(FID=id).all()
    result = []
    for row in query:
        result.append(
            courseTA_row_to_obj(row)
        )
    return jsonify({'status': 1, 'CourseTA': result}), 200


# route to get the FID given only the first name and lsat name of the instructor
@app.route(base_url + 'get_FID/<string:first>_<string:last>')
def get_FID(first, last):
    query1 = Instructor.query.filter_by(name_first=first).all()
    for row in query1:
        temp = instructor_row_to_obj(row)
        if temp['name_last'] == last:
            return jsonify({'status': 1, 'Instructor': temp}), 200
    return jsonify({'status': 2}), 500


#route to get the SID given only the first name and the last name of the student
@app.route(base_url + 'get_SID/<string:first>_<string:last>')
def get_SID(first, last):
    query1 = Student.query.filter_by(name_first=first).all()
    for row in query1:
        temp = student_row_to_obj(row)
        if temp['name_last'] == last:
            return jsonify({'status': 1, 'Student': temp}), 200
    return jsonify({'status': 2}), 500


# Can we do 'login/<int:id>', row = ...filter_by(ID=id).first(), and then check if the inputted password matches the one in the db instead?
# then we would return the id number to JS and save it there in a var to use for other GET requests?
#this route is for login, Default is a get request
@app.route(base_url+'login/<int:id>', methods=['POST'])
def login(id):
    user = User(**request.json)
    row1 = Instructor.query.filter_by(ID=id).first()
    row2 = Student.query.filter_by(ID=id).first()
    if row1 is not None:
        if user.ID == row1.ID:
            if user.password == row1.password:
                return jsonify({'status': 1, 'User': instructor_row_to_obj(row1)}), 200
    if row2 is not None:
        if user.ID == row2.ID:
            if user.password == row2.password:
                return jsonify({'status': 1, 'User': student_row_to_obj(row2)}), 200


@login_manager.user_loader
def user_loader(user_id):
    return User.query.get(user_id)



# route to get list of courses that don't have TA's
@app.route(base_url + 'get_courses_without_TAs', methods=['GET'])
def courses_wo_ta():
    query = CourseInstructor.query.filter_by(has_TA=False).all()

    result = []
    for row in query:
        result.append(courseInstructor_row_to_obj(row))
    return jsonify({"status": 1, "course": result}), 200


# route to get the list of TAs assigned to an instructor's classes
@app.route(base_url + 'get_assigned_TAs/<int:id>', methods=['GET'])
def assigned_TAs(id):
    query = CourseInstructor.query.filter_by(FID=id).filter_by(has_TA=True).all()

    result = []
    for row in query:
        result.append(courseInstructor_row_to_obj(row))
    return jsonify({"status": 1, "course": result}), 200


# route to get the list of TAs assigned to an instructor's classes
@app.route(base_url + 'get_assigned_CourseTAs/<int:id>', methods=['GET'])
def get_assigned_CourseTAs(id):
    query = CourseTA.query.filter_by(FID=id).filter_by(application_status="approved").all()

    result = []
    for row in query:
        result.append(courseTA_row_to_obj(row))
    return jsonify({"status": 1, "courseTA": result}), 200


#route to get items from the CourseTA table using the student id
@app.route(base_url + 'get_student_applications/<int:id>', methods=['GET'])
def get_student_applications(id):
    query = CourseTA.query.filter_by(SID=id).all()
    result = []
    for row in query:
        result.append(courseTA_row_to_obj(row))
    return jsonify({"status": 1, "courseTA": result}), 200


# route to update status code of TAs application. Also sets all other applications from the student to "dismissed"
@app.route(base_url + 'accept_app/<int:id>/<course_name>/<int:fid>', methods=['POST'])
def accept_app(id, course_name, fid):
    query1 = CourseTA.query.filter_by(SID=id).all()

    result = []
    for row in query1:
        # temp = courseTA_row_to_obj(row)
        if row.course_name == course_name:
            if row.FID == fid:
                row.application_status = "approved"
                query2 = CourseInstructor.query.filter_by(course_name=row.course_name).filter_by(FID=row.FID).first()
                # temp2 = courseInstructor_row_to_obj(query2)
                query2.has_TA = True
            else:
                row.application_status = "dismissed"
        result.append(courseTA_row_to_obj(row))
    
    db.session.commit()

    return jsonify({"status": 1, "application": result}), 200 # jsonify({"status":1, "course_instructor": temp2}), 200 this works I swear


# route to remove an application for CourseTA
@app.route(base_url + 'remove_app/<int:sid>/<course_name>/<int:fid>', methods=['DELETE'])
def remove_app(sid, course_name, fid):
    CourseTA.query.filter_by(course_name=course_name).filter_by(FID=fid).filter_by(SID=sid).delete()
    query = CourseInstructor.query.filter_by(has_TA=True).all()
    for row in query:
        if row.course_name == course_name:
            if row.FID == fid:
                row.has_TA = False
    db.session.commit()
    return jsonify({"status": 1}), 200


#row to object (create an object out of a row)
def student_row_to_obj(row):
    row = {
        "ID": row.ID,
        "name_first": row.name_first,
        "name_last": row.name_last,
        "email": row.email,
        "phoneNumber": row.phoneNumber,
        "major": row.major,
        "GPA": row.GPA,
        "gradDate": row.gradDate,
        "password": row.password
    }
    return row


#row to object (create an instructor object out of a row)
def instructor_row_to_obj(row):
    row = {
        "ID": row.ID,
        "name_first": row.name_first,
        "name_last": row.name_last,
        "email": row.email,
        "phoneNumber": row.phoneNumber,
        "department": row.department,
        "password": row.password
    }
    return row


#row to object (create a course object out of a row)
def course_row_to_obj(row):
    row = {
        "course_name": row.course_name,
        "title": row.title,
        "description": row.description
    }
    return row


#row to object (create a courseTA object out of a row)
def courseTA_row_to_obj(row):
    row = {
        "SID": row.SID,
        "FID": row.FID,
        "course_name": row.course_name,
        "grade": row.grade,
        "semester": row.semester,
        "application_date": row.application_date,
        "prior_TA": row.prior_TA,
        "application_status": row.application_status,
        "course_title": row.courseInfo.title,
        "course_description": row.courseInfo.description,
        "instructor_name_first": row.instructorInfo.name_first,
        "instructor_name_last": row.instructorInfo.name_last,
        "student_name_first": row.studentInfo.name_first,
        "student_name_last": row.studentInfo.name_last,
        "student_major": row.studentInfo.major,
        "student_GPA": row.studentInfo.GPA
    }
    return row


#row to object (create a student object from a row)
def studentTA_row_to_obj(row):
    row = {
        "course_name": row.course_name,
        "course_title": row.course_title.title,
        "name_first": row.studentInfo.name_first,
        "name_last": row.studentInfo.name_last
    }
    return row


#row to object (create a courseInstructor object from a row)
def courseInstructor_row_to_obj(row):
    row = {
        "FID": row.FID,
        "course_name": row.course_name,
        "title": row.courseInfo.title,
        "name_first": row.instructorInfo.name_first,
        "name_last": row.instructorInfo.name_last,
        "has_TA": row.has_TA
    }
    return row


#row to object (create a user object from a row)
def user_row_to_obj(row):
    row = {
        "ID": row.ID,
        "password": row.password
    }
    return row


def main():
    db.create_all()
    app.run()


if __name__ == '__main__':
    main()





