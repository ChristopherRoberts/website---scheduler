var Scheduler = (function() {

    var apiUrl = 'http://localhost:5000';

    var SchedulerSpace = 'placeholder';

    var student_template;
    var instructor_template;

    var scheduler;

    var student_load;
    var instructor_load;

    var student_create;
    var instructor_create;

    var instructor_addCourse;
    //var instructor_enterCourse;

    var student_edit;
    var instructor_edit;
    var student_profile;
    var instructor_profile;

    var courseList;
    var course_template;

    var studentTACourseList;
    var studentTACourse_template;

    var courseInstructorList;
    var CourseInstructor_template;

    var coursesWithoutTAList;
    var coursesWithoutTA_template;

    var instructorTACourseList;
    var instructorTACourse_template;

    var instructor_information;

    var studentApplicationsList;
    var studentApplications_template;
    

  /**
    * HTTP GET request 
    * @param  {string}   url       URL path, e.g. "/api/Scheduler"
    * @param  {function} onSuccess   callback method to execute upon request success (200 status)
    * @param  {function} onFailure   callback method to execute upon request failure (non-200 status)
    * @return {None}
    */
   
    var makeGetRequest = function(url, onSuccess, onFailure){
        $.ajax({
            type: 'GET',
            url: apiUrl + url,
            dataType: "json",
            success: onSuccess,
            error: onFailure
        });
    };
    
    /**
     * HTTP POST request
     * @param  {string}   url       URL path, e.g. "/api/Scheduler"
     * @param  {Object}   data      JSON data to send in request body
     * @param  {function} onSuccess   callback method to execute upon request success (200 status)
     * @param  {function} onFailure   callback method to execute upon request failure (non-200 status)
     * @return {None}
     */
    var makePostRequest = function(url, data, onSuccess, onFailure) {
        $.ajax({
            type: 'POST',
            url: apiUrl + url,
            data: JSON.stringify(data),
            contentType: "application/json",
            dataType: "json",
            success: onSuccess,
            error: onFailure
        });
    };

    /**
     * HTTP DELETE request
     * @param {string} url
     * @param {Object} data
     * @param {function} onSuccess
     * @param {function} onFailure
     * @return {None} 
     */
    var makeDeleteRequest = function(url, onSuccess, onFailure) {
        $.ajax({
            type: 'DELETE',
            url: apiUrl + url,
            contentType: "application/json",
            dataType: "json",
            success: onSuccess,
            error: onFailure
        });
    };

    //gets the ID of the current person logged in, student or instructor
    var getID = function (e) {
        logged_in_ID = document.cookie;
        var realID = logged_in_ID.split("=");
        logged_in_ID = realID[1];
    };

    //used to edit the student profile
    var StudentEditHandler = function (e) {
        console.log("entering StudentEditHandler");

        // 1. get data from backend
        // 2. display data to user
        // 3. return / save data and send to backend after clicking 'done' (below function)

        var getSuccess = function(old_data) {
            // console.log(data);
            // console.log($(this));
            // $('firstName') = data.Student.init_firstName;

            // load all the students info and insert it into the form.
            $('#firstName').val(old_data.Student.name_first);
            $('#lastName').val(old_data.Student.name_last);
            $('#IDNumber').val(old_data.Student.ID);
            $('#email').val(old_data.Student.email);
            $('#phoneNumber').val(old_data.Student.phoneNumber);
            $('#major').val(old_data.Student.major);
            $('#GPA').val(old_data.Student.GPA);
            $('#expectedGraduationDate').val(old_data.Student.gradDate);

            // user clicks 'done' button.
            student_edit.on('click', ".student_editButton", function (e) {
                e.preventDefault ();
        
                console.log("enter student edit function");

                var postSuccess = function(data) {
                    console.log(data.student.ID);
                    logged_in_ID = data.student.ID;
                    document.cookie = "ID=" + logged_in_ID + ";path=studentProfile.html";
                    window.location.href = "studentProfile.html";
                };

                var postFailure = function() {
                    console.error('could not edit student account')
                };

                // user edits data
                var init_firstName = $("#firstName").val();
                var init_lastName = $("#lastName").val();
                var init_wsuID = parseInt($("#IDNumber").val());
                var init_email = $("#email").val();
                var init_phoneNumber = parseInt($("#phoneNumber").val());
                var init_major = $("#major").val();
                var init_gpa = parseFloat($("#GPA").val());
                var init_gradDate = $("#expectedGraduationDate").val();
                
                // var init_priorTA = $(/*prior TAship goes here using # */).val();

                var data = {ID: init_wsuID, name_first: init_firstName, name_last: init_lastName, 
                            email: init_email, major: init_major, GPA: init_gpa, gradDate: init_gradDate, 
                            phoneNumber: init_phoneNumber}; //fill in data from above
                var outputError = "";
                //start checking data with output error
                if (init_firstName.length <= 0 || init_firstName.length > 64)
                {
                    outputError = outputError.concat("Error: First name length error \n");
                }
                if (init_lastName.length <= 0 || init_lastName.length > 64)
                {
                    outputError = outputError.concat("Error: Last name length error \n");
                }
                if (init_wsuID.length <= 0 || init_wsuID.length > 64)
                {
                    outputError = outputError.concat("Error: WSU ID length error \n");
                }
                if (init_email.length <= 0 || init_email.length > 64)
                {
                    outputError = outputError.concat("Error: Email length error \n");
                }
                if (init_phoneNumber.length <= 0 || init_phoneNumber.length > 64)
                {
                    outputError = outputError.concat("Error: Phone number length error \n");
                }
                if (init_major.length <= 0 || init_major.length > 64)
                {
                    outputError = outputError.concat("Error: Major length error \n");
                }
                if (init_gpa.length <= 0 || init_gpa.length > 64)
                {
                    outputError = outputError.concat("Error: GPA length error \n");
                }
                if (init_gradDate.length <= 0 || init_gradDate.length > 64)
                {
                    outputError = outputError.concat("Error: Graduation Date length error \n");
                }

                //make post request
                if (outputError.length == 0)
                {
                    console.log("code works until here");
                    var req = "/api/edit_student/" + logged_in_ID; // route to update a student account.
                    makePostRequest(req, data, postSuccess, postFailure);
                }
                else
                {
                    alert(outputError);
                }
            });
        };
        var getFailure = function() { 
            console.error('could not get student account'); 
        };
        getID();
        var req = "/api/get_student/" + logged_in_ID; // + the id number of the student. not sure how to get this yet.
        makeGetRequest(req, getSuccess, getFailure); // GET request to display info. in the getSuccess function is where we will display the info and then initiate a post request after user updates info and hits the "done" button.
    };

    //used to edit the instructor profile
    var InstructorEditHandler = function (e) {
        console.log("entering InstructorEditHandler");

        // 1. get data from backend
        // 2. display data to user
        // 3. return / save data and send to backend after clicking 'done' (below function)

        var getSuccess = function(old_data) {
            // console.log(data);
            // console.log($(this));
            // $('firstName') = data.Instructor.init_firstName;

            // load all the instructors info and insert it into the form.
            $('#firstName').val(old_data.Instructor.name_first);
            $('#lastName').val(old_data.Instructor.name_last);
            $('#IDNumber').val(old_data.Instructor.ID);
            $('#email').val(old_data.Instructor.email);
            $('#phoneNumber').val(old_data.Instructor.phoneNumber);
            $('#department').val(old_data.Instructor.department);

            // user clicks 'done' button.
            instructor_edit.on('click', ".instructor_editButton", function (e) {
                e.preventDefault ();
        
                console.log("enter instructor edit function");

                var postSuccess = function(data) {
                    console.log(data.instructor.ID);
                    logged_in_ID = data.instructor.ID;
                    document.cookie = "ID=" + logged_in_ID + ";path=instructorProfile.html"
                    window.location.href = "instructorProfile.html";
                };

                var postFailure = function() {
                    console.error('could not edit instructor account')
                };

                // user edits data
                var init_firstName = $("#firstName").val();
                var init_lastName = $("#lastName").val();
                var init_wsuID = parseInt($("#IDNumber").val());
                var init_email = $("#email").val();
                var init_phoneNumber = parseInt($("#phoneNumber").val());
                var init_department = $("#department").val();
                
                // var init_priorTA = $(/*prior TAship goes here using # */).val();

                var data = {ID: init_wsuID, name_first: init_firstName, name_last: init_lastName, 
                            email: init_email, department: init_department, 
                            phoneNumber: init_phoneNumber}; //fill in data from above
                var outputError = "";
                //start checking data with output error
                if (init_firstName.length <= 0 || init_firstName.length > 64)
                {
                    outputError = outputError.concat("Error: First name length error \n");
                }
                if (init_lastName.length <= 0 || init_lastName.length > 64)
                {
                    outputError = outputError.concat("Error: Last name length error \n");
                }
                if (init_wsuID.length <= 0 || init_wsuID.length > 64)
                {
                    outputError = outputError.concat("Error: WSU ID length error \n");
                }
                if (init_email.length <= 0 || init_email.length > 64)
                {
                    outputError = outputError.concat("Error: Email length error \n");
                }
                if (init_phoneNumber.length <= 0 || init_phoneNumber.length > 64)
                {
                    outputError = outputError.concat("Error: Phone number length error \n");
                }
                if (init_department.length <= 0 || init_department.length > 64)
                {
                    outputError = outputError.concat("Error: Major length error \n");
                }

                //make post request
                if (outputError.length == 0)
                {
                    console.log("code works until here");
                    var req = "/api/edit_instructor/" + logged_in_ID; // route to update an instructor account.
                    makePostRequest(req, data, postSuccess, postFailure);
                }
                else
                {
                    alert(outputError);
                }
            });
        };
        var getFailure = function() { 
            console.error('could not get instructor account'); 
        };
        getID();
        var req = "/api/get_instructor/" + logged_in_ID; // + the id number of the instructor. not sure how to get this yet.
        makeGetRequest(req, getSuccess, getFailure); // GET request to display info. in the getSuccess function is where we will display the info and then initiate a post request after user updates info and hits the "done" button.
    };


    //used to show the student profile
    var StudentProfile = function (e) {
        console.log("entered StudentProfile function");


        // 1. look up data of user based on ID number
        // 2. display the user's data back to them


        // var newElem = $(student_template);
        // newElem.find('#firstName')
        // $('#firstName')[0].innerHTML = "new first name"
        // replace all the items from the html template with items from the db

        var onSuccess = function(data) { // data successfully contains the json info for the student profile.
            // console.log(data.Student.GPA); // this prints out the students GPA to the console. we can use this same method for all of the profile info
            // console.log($('#firstName')[0].innerHTML); // this prints out the placeholder text from the page.
            $('#firstName')[0].innerHTML = data.Student.name_first;
            $('#lastName')[0].innerHTML = data.Student.name_last;
            $('#IDNumber')[0].innerHTML = data.Student.ID;
            $('#email')[0].innerHTML = data.Student.email;
            $('#phoneNumber')[0].innerHTML = data.Student.phoneNumber;
            $('#major')[0].innerHTML = data.Student.major;
            $('#GPA')[0].innerHTML = data.Student.GPA;
            $('#expectedGraduationDate')[0].innerHTML = data.Student.gradDate;
        };

        var onFailure = function() {
            console.error("error loading profile")
        };
        getID();
        var req = "/api/get_student/" + logged_in_ID; // + the id number of the student. not sure how to get this yet.
        makeGetRequest(req, onSuccess, onFailure);
    };


    //used to load the instructor profile
    var InstructorProfile = function (e) {
        console.log("entered InstructorProfile function");

        // 1. look up data of user based on ID number
        // 2. display the user's data back to them


        var onSuccess = function(data) {
            $('#firstName')[0].innerHTML = data.Instructor.name_first;
            $('#lastName')[0].innerHTML = data.Instructor.name_last;
            $('#IDNumber')[0].innerHTML = data.Instructor.ID;
            $('#email')[0].innerHTML = data.Instructor.email;
            $('#phoneNumber')[0].innerHTML = data.Instructor.phoneNumber;
            $('#department')[0].innerHTML = data.Instructor.department;
            // console.log(data.Instructor);
        };

        var onFailure = function() {
            console.error("error loading profile")
        };
        getID();
        console.log(logged_in_ID);
        var req = "/api/get_instructor/" + logged_in_ID; // + the id number of the instructor. not sure how to get this yet.
        makeGetRequest(req, onSuccess, onFailure);
    };

    //used to load the home page and handle the loggin of students and instructors
    var IndexHandler = function (e) {
        //this is where we will handle the login buttons being pushed
        console.log("entered IndexHandler function");

        //login button handling for student
        student_load.on("click", ".student_login", function (e) {
            console.log("student login button was clicked");
            e.preventDefault ();

            var onSuccess = function(data) {
                console.log("success, here is data: ", data);
                //need to check to see if information is in our profile list
                // logged_in_ID = data.wherever_the_id_is_in_this_thing
                // redirect to courseList page
                //logged_in_ID = data.ID;
                document.cookie = "ID=" + data.User.ID + ";path=studentProfile.html";
                window.location.href = "studentProfile.html";
            };
            var onFailure = function() {
                //console.error("login unsuccessful, username or password may not be correct"); 
                alert("username or password may be incorrect");               
            };

            var username = $("#studentName").val();
            var password = $("#studentPassword").val();

            var data = {ID: parseInt(username), password: password}; // check the data against the db somehow

            var outputError = "";

            if (username.length <= 0)
            {
                outputError = outputError.concat("Must enter a username\n");
            }
            if (password.length <= 0)
            {
                outputError = outputError.concat("Must enter a password\n");
            }

            if (outputError.length == 0)
            {
                //search from list of current usernames to see if it matches
                //console.log("code is working up until this point, need to add more");
                var req = "/api/login/" + username;
                makePostRequest(req, data, onSuccess, onFailure);
            }
            else
            {
                alert(outputError);
            }
        });

        //instructor login button handling
        console.log("entered IndexHandler function");
        instructor_load.on("click", ".instructor_login", function (e) {
            console.log("instructor login button was clicked");
            e.preventDefault ();

            //do i need these if I am not making a post request
            var onSuccess = function(data) {
                console.log("success, here is data: ", data);
                //need to check to see if information is in our profile list
                // logged_in_ID = data.wherever_the_id_is_in_this_thing
                // redirect to courseList page
                //logged_in_ID = data.ID;
                document.cookie = "ID=" + data.User.ID + ";path=instructorProfile.html";
                window.location.href = "instructorProfile.html";
            };
            var onFailure = function() {
                //console.error("login unsuccessful, username or password may not be correct"); 
                alert("username or password may be incorrect");            
            };

            var username = parseInt($("#instructorName").val());
            var password = $("#instructorPassword").val();
            // console.log(username);
            // console.log(password);
            var outputError = "";
            var data = {ID:username, password:password};

            if (username.length <= 0)
            {
                outputError = outputError.concat("Must enter a username\n");
            }
            if (password.length <= 0)
            {
                outputError = outputError.concat("Must enter a password\n");
            }

            if (outputError.length == 0)
            {
                //search from list of current usernames to see if it matches
                console.log("code is working up until this point, need to add more");
                var req = "/api/login/" + username;
                makePostRequest(req, data, onSuccess, onFailure);
            }
            else
            {
                alert(outputError);
            }
        });

    };


    //create the student profile
    var StudentCreateHandler = function (e) {
        //do we need to hide any forms initially? - i dont think so
        console.log("entering StudentCreateHandler");

        //-------------------STUDENT-------------------------
        //  'Create account' button
        student_create.on('click', ".student_createButton", function (e) {
            e.preventDefault ();

            console.log("enter student create function");

            //onSuccess and onFailure functions
            var onSuccess = function(data) {
                //need to figure out how to add to profile list below here
                // console.log(data);
                // console.log($(this));
                logged_in_ID = data.student.ID;
                document.cookie = "ID=" + logged_in_ID + ";path=studentProfile.html";
                window.location.href = "studentProfile.html";
            };
            var onFailure = function() { 
                console.error('could not add student account'); 
            };

            //creating account information
            var init_firstName = $("#firstName").val();
            var init_lastName = $("#lastName").val();
            var init_wsuID = parseInt($("#IDNumber").val());
            var init_email = $("#email").val();
            var init_phoneNumber = parseInt($("#phoneNumber").val());
            var init_major = $("#major").val();
            var init_gpa = parseFloat($("#GPA").val());
            var init_gradDate = $("#expectedGraduationDate").val();
            var init_password = $("#studentPassword").val();
            var init_confirmPassword = $("#studentConfirm").val();
            
            // var init_priorTA = $(/*prior TAship goes here using # */).val();

            var data = {ID: init_wsuID, name_first: init_firstName, name_last: init_lastName, 
                        email: init_email, major: init_major, GPA: init_gpa, gradDate: init_gradDate, 
                        phoneNumber: init_phoneNumber, password: init_password}; //fill in data from above
            var outputError = "";
            //start checking data with output error
            if (init_firstName.length <= 0 || init_firstName.length > 64)
            {
                outputError = outputError.concat("Error: First name length error \n");
            }
            if (init_lastName.length <= 0 || init_lastName.length > 64)
            {
                outputError = outputError.concat("Error: Last name length error \n");
            }
            if (init_wsuID.length <= 0 || init_wsuID.length > 64)
            {
                outputError = outputError.concat("Error: WSU ID length error \n");
            }
            if (init_email.length <= 0 || init_email.length > 64)
            {
                outputError = outputError.concat("Error: Email length error \n");
            }
            if (init_phoneNumber.length <= 0 || init_phoneNumber.length > 64)
            {
                outputError = outputError.concat("Error: Phone number length error \n");
            }
            if (init_major.length <= 0 || init_major.length > 64)
            {
                outputError = outputError.concat("Error: Major length error \n");
            }
            if (init_gpa.length <= 0 || init_gpa.length > 64)
            {
                outputError = outputError.concat("Error: GPA length error \n");
            }
            if (init_gradDate.length <= 0 || init_gradDate.length > 64)
            {
                outputError = outputError.concat("Error: Graduation Date length error \n");
            }
            if (init_password != init_confirmPassword)
            {
                outputError = outputError.concat("Error: Passwords do not match \n");
            }

            //make post request
            if (outputError.length == 0)
            {
                console.log("code works until here");
                //post request doesnt work, need to fix
                makePostRequest("/api/create_student", data, onSuccess, onFailure);
            }
            else
            {
                alert(outputError);
            }
        });
    };


    //used to create an instructor
    var InstructorCreateHandler = function (e) {
        console.log("entering InstructorCreateHandler");
        //--------------------INSTRUCTOR------------------------
        //teacher save profile
        instructor_create.on('click', ".instructor_createButton", function (e) {
            console.log("inside of instructor create function");
            e.preventDefault ();

            //onSuccess and onFailure functions
            var onSuccess = function(data) {
                //need to figure out how to add to larger list below here
                console.log("success");
                // console.log(data);
                logged_in_ID = data.instructor.ID;
                document.cookie = "ID=" + logged_in_ID + ";path=instructorProfile.html";
                window.location.href = "instructorProfile.html";
            };
            var onFailure = function() { 
                console.error('could not add instructor account'); 
            };

            //creating account information
            var init_firstName = $("#firstName").val();
            var init_lastName = $("#lastName").val();
            var init_wsuID = parseInt($("#IDNumber").val());
            var init_email = $("#email").val();
            var init_phoneNumber = parseInt($("#phoneNumber").val());
            var init_department = $("#department").val();
            var init_password = $("#instructorPassword").val();
            var init_confirmPassword = $("#instructorConfirm").val();
            
            var data = {ID: init_wsuID, name_first: init_firstName, name_last: init_lastName, 
                        email: init_email, phoneNumber: init_phoneNumber, department: init_department,
                        password: init_password}; //fill in data from above
            var outputError = "";
            //start checking data with output error
            if (init_firstName.length <= 0 || init_firstName.length > 64)
            {
                outputError = outputError.concat("Error: First name length error \n");
            }
            if (init_lastName.length <= 0 || init_lastName.length > 64)
            {
                outputError = outputError.concat("Error: Last name length error \n");
            }
            if (init_wsuID.length <= 0 || init_wsuID.length > 64)
            {
                outputError = outputError.concat("Error: WSU ID length error \n");
            }
            if (init_email.length <= 0 || init_email.length > 64)
            {
                outputError = outputError.concat("Error: Email length error \n");
            }
            if (init_phoneNumber.length <= 0 || init_phoneNumber.length > 64)
            {
                outputError = outputError.concat("Error: Phone number length error \n");
            }
            if (init_department.length <= 0 || init_department.length > 64)
            {
                outputError = outputError.concat("Error: Department length error \n");
            }
            if (init_password != init_confirmPassword)
            {
                outputError = outputError.concat("Error: Passwords do not match \n");
            }

            //make post request
            if (outputError.length == 0)
            {
                console.log("code works up until this point, need to add");
                //post request doenst work right now
                makePostRequest('/api/create_instructor', data, onSuccess, onFailure);
            }
            else
            {
                alert(outputError);
            }
        });
    };

    //used by instructor to add a new course to the course database
    var AddCourseHandler = function (e) {
        console.log("entering addCourseHandler");
        console.log(instructor_addCourse);
        //handling for clicking the "add course" button
        instructor_addCourse.on("click", ".addCourseButton", function (e) {
            console.log("add course button was clicked");
            e.preventDefault();

            //onSuccess and onFailure functions
            var onSuccess = function(data) {
                console.log("success");
                // console.log(data);
                window.location.href = "courseList.html";
            };
            var onFailure = function() { 
                console.error('could not add course'); 
            };

            var courseName = $("#courseName").val();
            var courseTitle = $("#courseTitle").val();
            var courseDescription = $("#courseDescription").val();

            var data = {course_name: courseName, title: courseTitle, description: courseDescription}

            var outputError = "";
            if (courseName.length <= 0)
            {
                outputError = outputError.concat("Must enter a course name \n");
            }
            if (courseTitle.length <= 0)
            {
                outputError = outputError.concat("Must enter a course title \n");
            }
            if (courseDescription.length <= 0)
            {
                outputError = outputError.concat("Must enter a course description \n");
            }

            if (outputError.length == 0)
            {
                console.log("code works up until this point, need to add");
                //need to add something here that will add the data. Is it an actual post request?
                makePostRequest('/api/create_course', data, onSuccess, onFailure);
            }
            else
            {
                alert(outputError);
            }

        });

    };

    //used by instructor to handle the on-click function to add the clicked
    //on course to the instructorCourse table with the logged-in instructor ID
     var CourseListHandler = function (e) {
        console.log("entering courseListHandler");

        courseList.on("click", ".courseListButton", function (e) {
            e.preventDefault();
            console.log("inside of click function of CourseListHandler");
            var courseName = $(this).parent()[0].firstElementChild.innerHTML;
            //console.log(courseName);
            getID();
            var FID = parseInt(logged_in_ID);
            //console.log(FID);

            var data = {course_name: courseName, FID: FID};
            // console.log(data);
            var onSuccess = function (data) {
                console.log("success");
                // console.log(data);
                alert("Course was successfuly added!")
            };
            var onFailure = function () {
                console.error("could not add course");
                alert("Failed to add course");
            };
            
            var req = '/api/create_courseInstructor';
            makePostRequest(req, data, onSuccess, onFailure);

        });

    };

    //this is a "im winging it" part. - christopher
    var InsertCourseHandler = function(course, beginning){
        console.log("entering insertCoursehandler");
        var newElem = $(course_template);
        // console.log(newElem);
        // console.log(course.course_name);
        newElem.find('.name').text(course.course_name);
        newElem.find('.title').text(course.title);
        newElem.find('.description').text(course.description);
        //i think this 'if' is adding the new element back to course which
        //is where we grabbed it from. i could be wrong tho.
        if (beginning) {
            courseList.prepend(newElem);
        } else {
            courseList.append(newElem);
        }

        //we're successfuly getting ALL of the courses from the database
        //now just to print them to the screen
    };

    //displays all courseInstructor items in the table
    var DisplayCourseListHandler = function(e) {
        console.log("entering DisplayCourseListHandler");
        var onSuccess = function(data) {
            for (i = 0; i < data.Course.length; i++)
            {
                console.log(data.Course[i]);
                InsertCourseHandler (data.Course[i], true);
                //im not sure the funciton of this bool ^
            }
        };

        var onFailure = function() {
            console.error('display course list failed');
        };

        var req = "/api/get_all_courses"
        makeGetRequest(req, onSuccess, onFailure);
    };
    
    //this function is used by students to apply to available courses
    var StudentTAListHandler = function(e) {
        console.log("entering StudentTAListHandler");
        studentTACourseList.on("click", ".applyButton", function(e) {
            e.preventDefault();
            console.log("inside click function");

            var courseName = $(this).parent()[0].firstElementChild.innerHTML
            var instructor = $(this).parent()[0].firstElementChild.nextElementSibling.nextElementSibling.innerHTML;
            var res = instructor.split(" ");
            getID();
            var SID = parseInt(logged_in_ID);
            console.log(courseName);
            console.log(SID);

            
            var getSuccessInstructor = function (instructorInfo) {
                console.log("enterting getSuccessInstructor");
                var getSuccessStudent = function (studentInfo) {
                    console.log("entering getSuccessStudent");
                    console.log(instructorInfo);
                    console.log(studentInfo);
                    // console.log(instructorInfo.Instructor.ID);
                    var todaysDate = new Date();
                    var result = [];
                    result[0] = todaysDate.getDate();
                    result[1] = todaysDate.getMonth() + 1;
                    result[2] = todaysDate.getFullYear();
                    var realDate = '';
                    realDate = result[0] + '/' + result[1] + '/' + result[2];
                    console.log(realDate);
                    data = {SID: SID, FID: instructorInfo.Instructor.ID, course_name: courseName, 
                        grade: studentInfo.Student.GPA, application_date: realDate}
                    // console.log(data);
                    var postSuccess = function(data) {
                        console.log("entered postSuccess");
                        // console.log(data);
                        alert("Successfuly applied!");

                    };
                    var postFailure = function() {
                        //console.error("post request inside of StudentTAListHandler failed");
                        alert("failed to apply");
                    }
                    req = "/api/create_courseTA";
                    makePostRequest(req, data, postSuccess, postFailure);
                };
                var getFailureStudent = function () {
                    console.error("getReq for student in SstudentTAListHandler failed");
                };
                var req = "/api/get_student/" + SID;
                makeGetRequest(req, getSuccessStudent, getFailureStudent);
            };
            var getFailureInstructor = function() {
                console.error("getReq for instructor in studentTAListHandler failed");
            }
            var req = "/api/get_FID/" + res[0] + "_" + res[1];
            makeGetRequest(req, getSuccessInstructor, getFailureInstructor);
        })
    };


    //insert a single course into the list of courses without a TA
    var InsertStudentTAListHandler = function(course, beginning){
        console.log("entering InsertStudentTAListHandler");
        var newElem = $(studentTACourse_template);
        newElem.find('.course_name').text(course.course_name);
        newElem.find('.course_title').text(course.title);
        newElem.find('.instructor_name').text(course.name_first + ' ' + course.name_last);
        if (beginning) {
            studentTACourseList.prepend(newElem);
        } else {
            studentTACourseList.append(newElem);
        }
    };

    // this should display all courses without a TA
    var DisplayCoursesWithoutTAList = function (e) {
        console.log("entering DisplayCoursesWithoutTAList");
        var onSuccess = function(data) {
            console.log("entering onSuccess");
            // console.log(data);
            for(i = 0; i < data.course.length; i++)
            {
                //console.log(data.course[i]);
                InsertStudentTAListHandler(data.course[i], true);
            }
        };

        var onFailure = function() {
            console.log("DisplayCoursesWithoutTAList failed");
        };

        var req = "/api/get_courses_without_TAs"
        makeGetRequest(req, onSuccess, onFailure);
    };

    //Should insert a course taught by an instructor
    var InsertInstructorCoursesHandler = function(course, beginning) {
        console.log("entering InsertInstructorCoursesHandler");
        var newElem = $(CourseInstructor_template);
        newElem.find('.course').text(course.course_name);
        newElem.find('.instructor_name').text(course.name_first + ' ' + course.name_last);
        newElem.find('.course_title').text(course.title);
        if(beginning) {
            courseInstructorList.prepend(newElem);
        } else {
            courseInstructorList.append(newElem);
        }
    };

    //Should display all courses taught by an instructor
    var DisplayInstructorCourses = function(e) {
        console.log("enterting DisplayInstructorCourses");
        var onSuccess = function(data) {
            // console.log("entering onSuccess");
            // console.log(data.CourseInstructor);
            for(i = 0; i < data.CourseInstructor.length; i++)
            {
                InsertInstructorCoursesHandler(data.CourseInstructor[i]);
            }
        };
        var onFailure = function() {
            console.log("DisplayInstructorCourses failed");
        };

        getID();
        console.log(logged_in_ID);
        var req = "/api/get_Courseinstructor/" + logged_in_ID;
        makeGetRequest(req, onSuccess, onFailure);
    };

    //used by displayinstructorTAcourselist to insert a single course to the page
    var InsertInstructorTACourseHandler = function(CourseTA, beginning){
        console.log("enterting InsertInstructorTACourseHandler");
        var newElem = $(instructorTACourse_template);
        newElem.find('.course_name').text(CourseTA.course_name);
        newElem.find('.course_title').text(CourseTA.course_title);
        newElem.find('.instructor_name').text(CourseTA.instructor_name_first + ' ' + CourseTA.instructor_name_last);
        newElem.find('.student_name').text(CourseTA.student_name_first + ' ' + CourseTA.student_name_last);
        newElem.find('.student_grade').text(CourseTA.grade);
        newElem.find('.student_semester').text(CourseTA.semester);
        newElem.find('.student_prior_TA').text(CourseTA.prior_TA);
        newElem.find('.application_status').text(CourseTA.application_status);
        newElem.find('.student_applied_date').text(CourseTA.application_date);
        if(beginning) {
            instructorTACourseList.prepend(newElem);
        } else {
            instructorTACourseList.append(newElem);
        }
    };

    //used by instructor to view all TA applications for their classes
    var DisplayInstructorTACourseList = function(e) {
        console.log("enterting DisplayInstructorTACourseList");
        var onSuccess = function(data) {
            // console.log(data)
            console.log("enterting onSuccess");
            for(i = 0; i < data.CourseTA.length; i++)
            {
                // console.log("entered the for loop");
                // console.log(data.CourseTA[i]);
                InsertInstructorTACourseHandler(data.CourseTA[i]);
            }
        };
        var onFailure = function() {
            console.log("DisplayInstructorTACourseList failed");
        };
        getID();
        console.log(logged_in_ID);
        var req = "/api/get_instructor_courses/" + logged_in_ID;
        makeGetRequest(req, onSuccess, onFailure);
    };

    //used by instructor to approve a TA application
    var ApproveTAShipHandler = function (e) {
        console.log("entering ApproveTAShipHandler");
        instructorTACourseList.on("click", ".approveButton", function(e) {
            e.preventDefault();
            console.log("inside click function");
            var courseName = $(this).parent()[0].firstElementChild.innerHTML;
            console.log(courseName);
            var student = $(this).parent()[0].firstElementChild.nextElementSibling.nextElementSibling.nextElementSibling.innerHTML;
            console.log(student);
            var res = student.split(" ");
            var req = "/api/get_SID/" + res[0] + "_" + res[1];
            var temp = $(this).parent()[0];

            var getonSuccess = function(studentInfo) {
                console.log("entering getonSuccess")
                SID = studentInfo.Student.ID;
                console.log(SID);
                console.log(logged_in_ID);
                data = {}
                // console.log(data);
                var postonSuccess = function(data) {
                    console.log("entering postonSuccess");
                    // console.log(data);
                    $(temp).find(".application_status").text(data.application[0].application_status);
                    window.location.reload(true);
                };
                var postonFailure = function () {
                    console.error("post request for ApproveTAShipHandler failed");
                };
                var req = "/api/accept_app/" + SID + "/" + courseName + "/" + logged_in_ID;
                makePostRequest(req, data, postonSuccess, postonFailure);
            };
            var getonFailure = function () {
                console.error("get request for ApproveTAShipHandler failed");
            };
            var req = "/api/get_SID/" + res[0] + "_" + res[1];
            console.log(req);
            makeGetRequest(req, getonSuccess, getonFailure);
        });
    };

    // used by instructor to remove a TA application from their course
    var RemoveTAShipHandler = function (e) {
        console.log("entering RemoveTAShipHandler");
        instructorTACourseList.on("click", ".removeButton", function(e) {
            e.preventDefault();
            console.log("inside click function");
            var courseName = $(this).parent()[0].firstElementChild.innerHTML;
            console.log(courseName);
            var student = $(this).parent()[0].firstElementChild.nextElementSibling.nextElementSibling.nextElementSibling.innerHTML;
            console.log(student);
            var res = student.split(" ");
            var req = "/api/get_SID/" + res[0] + "_" + res[1];

            var getonSuccess = function(studentInfo) {
                console.log("entering getonSuccess")
                SID = studentInfo.Student.ID;
                console.log(SID);
                console.log(logged_in_ID);
                data = {}
                // console.log(data);
                var deleteonSuccess = function(data) {
                    console.log("entering deleteonSuccess");
                    window.location.reload(true);
                };
                var deleteonFailure = function () {
                    console.error("delete request for RemoveTAShipHandler failed");
                };
                var req = "/api/remove_app/" + SID + "/" + courseName + "/" + logged_in_ID;
                makeDeleteRequest(req, deleteonSuccess, deleteonFailure);
            };
            var getonFailure = function () {
                console.error("get request for RemoeveTAShipHandler failed");
            };
            var req = "/api/get_SID/" + res[0] + "_" + res[1];
            console.log(req);
            makeGetRequest(req, getonSuccess, getonFailure);
        });
    };

    //displays all of the students TA applications
    var DisplayStudentApplicationsList = function (e) {
        console.log("entered DisplayStudentApplicationsList");
        var onSuccess = function (data) {
            console.log("entering onSuccess");
            // console.log(data);
            for(i = 0; i < data.courseTA.length; i++)
            {
                InsertStudentTACourseHandler(data.courseTA[i]);
            }

        };
        var onFailure = function () {
            console.error("DisplayStudentApplicationsList failed");
        }
        getID();
        var req = "/api/get_student_applications/" + logged_in_ID;
        makeGetRequest(req, onSuccess, onFailure);
    };

    //used by student get each entry of their TA applications
    var InsertStudentTACourseHandler = function(CourseTA, beginning){
        console.log("enterting InsertStudentTACourseHandler");
        var newElem = $(studentApplications_template);
        newElem.find('.course_name').text(CourseTA.course_name);
        newElem.find('.course_title').text(CourseTA.course_title);
        newElem.find('.instructor_name').text(CourseTA.instructor_name_first + ' ' + CourseTA.instructor_name_last);
        newElem.find('.student_name').text(CourseTA.student_name_first + ' ' + CourseTA.student_name_last);
        newElem.find('.student_grade').text(CourseTA.grade);
        newElem.find('.student_semester').text(CourseTA.semester);
        newElem.find('.student_prior_TA').text(CourseTA.prior_TA);
        newElem.find('.application_status').text(CourseTA.application_status);
        newElem.find('.student_applied_date').text(CourseTA.application_date);
        if(beginning) {
            studentApplicationsList.prepend(newElem);
        } else {
            studentApplicationsList.append(newElem);
        }
    };

    //used by student to cancel an existing application
    var CancelTAApplicationHandler = function (e) {
        console.log("entering CancelTAApplicationHandler")
        studentApplicationsList.on("click", ".cancelButton", function(e){
            e.preventDefault();
            console.log("inside of click function");
            var courseName = $(this).parent()[0].firstElementChild.innerHTML;
            console.log(courseName);
            var instructor = $(this).parent()[0].firstElementChild.nextElementSibling.nextElementSibling.innerHTML;
            console.log(instructor);
            var res = instructor.split(" ");

            var getonSuccess = function(instructorInfo) {
                console.log("entering getonSuccess")
                FID = instructorInfo.Instructor.ID;
                console.log(FID);
                console.log(logged_in_ID);
                data = {}

                var deleteonSuccess = function(data) {
                    console.log("entering deleteonSuccess");
                    window.location.reload(true);
                };
                var deleteonFailure = function () {
                    console.error("delete request for CancelTAApplicationHandler failed");
                };
                var req = "/api/remove_app/" + logged_in_ID + "/" + courseName + "/" + FID;
                makeDeleteRequest(req, deleteonSuccess, deleteonFailure);

            };
            var getonFailure = function () {
                console.error("get request for CancelTAApplicationHandler failed");
            };

            var req = "/api/get_FID/" + res[0] + "_" + res[1];
            console.log(req);
            makeGetRequest(req, getonSuccess, getonFailure);
        });
    };


    //start functions-------------------------------------
    var start_StudentEdit = function() {
        student_edit = $(".student_form_edit"); // should all of these be different variables?

        StudentEditHandler();
    };

    //used to edit the instructor profile
    var start_InstructorEdit = function() {
        instructor_edit = $(".instructor_form_edit");

        InstructorEditHandler();
    };

    //used to show the student profile
    var start_StudentProfile = function() {
        student_profile = $(".profile");
        student_template = $(".profile").outerHTML;

        StudentProfile();
    };

    //used to show the instructor profile
    var start_InstructorProfile = function() {
        instructor_profile = $(".profile");
        instructor_template = $(".profile").outerHTML;

        InstructorProfile();
    };

    /**
     * When the index is run, need to grab and initialize the student_index and instructor_index variables.
     * @return {None}
     */
    var start_index = function() {
        //scheduler = $();
        student_load = $(".student");
        instructor_load = $(".instructor");

        //call the function below to act on "clicks"
        IndexHandler();

        //this clears the scheduler? Do i need this? It was in smile.js
        //scheduler.html('');
    };

    //starter function for create
    var start_StudentCreate = function() {
        student_create = $(".student_form_create");

        StudentCreateHandler();

        //do i need to do anything else in this function?
    };
    
    var start_InstructorCreate = function() {
        instructor_create = $(".instructor_form_create");

        InstructorCreateHandler();

        //do i need to do anything else in this function?
    };

    var start_AddCourse = function() {
        instructor_addCourse = $(".addCourse");
        AddCourseHandler();
    };

    //used to display the course list
    var start_CourseList = function() {
        courseList = $(".outline");
        course_template = $('.outline .class')[0].outerHTML;
        courseList.html('');
        DisplayCourseListHandler();
        CourseListHandler();


    };
    
    //used to show the courses available to TA to students
    var start_StudentCourseTAList = function() {
        studentTACourseList = $(".outline");
        studentTACourse_template = $('.outline .class')[0].outerHTML;
        studentTACourseList.html('');
        DisplayCoursesWithoutTAList();
        StudentTAListHandler();
    };

    //call the function to show all courses owned by an instructor
    var start_CourseInstructorApplications = function() {
        courseInstructorList = $(".outline");
        CourseInstructor_template = $(".outline .class")[0].outerHTML;
        courseInstructorList.html('');
        DisplayInstructorCourses();
    };

    //used to show the courseTa applications to the instructor and approve/remove them
    var start_InstructorCourseTAList = function () {
        instructorTACourseList = $(".outline");
        instructorTACourse_template = $('.outline .class')[0].outerHTML;
        instructorTACourseList.html('');
        DisplayInstructorTACourseList();
        ApproveTAShipHandler();
        RemoveTAShipHandler();
    };

    //used to show the current student's applications and cancel them
    var start_StudentTAApplications = function () {
        studentApplicationsList = $('.outline');
        studentApplications_template = $(".outline .class")[0].outerHTML;
        studentApplicationsList.html('');
        DisplayStudentApplicationsList();
        CancelTAApplicationHandler();
    }

    // PUBLIC METHODS
    // any private methods returned in the hash are accessible via Smile.key_name, e.g. Smile.start()
    return {
        start_StudentEdit: start_StudentEdit,
        start_InstructorEdit: start_InstructorEdit,
        start_StudentProfile: start_StudentProfile,
        start_InstructorProfile: start_InstructorProfile,
        start_index: start_index,
        start_StudentCreate: start_StudentCreate,
        start_InstructorCreate: start_InstructorCreate,
        start_AddCourse: start_AddCourse,
        start_CourseList: start_CourseList,
        start_StudentCourseTAList: start_StudentCourseTAList,
        start_CourseInstructorApplications: start_CourseInstructorApplications,
        start_InstructorCourseTAList: start_InstructorCourseTAList,
        start_StudentTAApplications: start_StudentTAApplications
    };
    
})();

